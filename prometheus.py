#!venv/bin/python
from mimetypes import init
import collectd
import requests
import yaml
import re
import pprint
from prometheus_client.parser import text_string_to_metric_families

collectdConfig={}

pp = pprint.PrettyPrinter(indent=4)
"""
This version is updated to scrape and format data from prometheus ! 
"""

class prometheusCollect:

    def __init__(self, path):
        self.path = path 

    def collect(self):
            """
            Get metrics from prometheus 
            Only Counter & Gauge !
            Summary and histogram are not handled
            Output : 
                {counter : [sample,...], gauge : [sample,...]}
            """
            response = requests.get(collectdConfig['path'][0])
            metrics = response.text
            
            self.promCollect = {}
            for family in text_string_to_metric_families(metrics):
                for sample in family.samples:
                    if family.type not in ('summary', 'histogram'):
                        #if re.search(conf_colld['filter'][0], sample.name):
                        if re.search(r'.*', sample.name):
                            if family.type in self.promCollect:
                                self.promCollect[family.type].append(sample)
                            else:
                                self.promCollect[family.type]=[sample]
            return self.promCollect
    
    @staticmethod
    def applyRules(conf, labels):
        """
            Sample : 
            {'load_balancer': 'app/(.*)/.*,\\g<1>'}, 
            {'load_balancer': 'app/TOTO/f8be9abf9074a161', 'instance': '', 'job': 'aws_applicationelb'})
            Conf could be : 
                replace     : {'load_balancer': 'app/(.*)/.*,\\g<1>'}
                set         : {'load_balancer' : 1 } 
                freetext    : sample text 
        """
        if type(conf) is dict and conf.values()[0] != 1:
            confLabelName = conf.keys()[0]
            rex = conf.values()[0]
            if confLabelName in labels:
                return re.sub(rex.split(',')[0], rex.split(',')[1], labels[conf.keys()[0]])
            return None
            
        elif type(conf) is dict and conf.values()[0] == 1:
            if conf.keys()[0] in labels:
                return labels[conf.keys()[0]]
            return None

        return conf
    
    @staticmethod
    def removeSign(label):
        #{<job> : regex}
        if type(label) is dict:
            if re.search("^<|>$", label.keys()[0]):
                return { re.sub("^<|>$", "", label.keys()[0]) : label.values()[0]}
        else:
            if re.search("^<|>$", label):
                return { re.sub("^<|>$","",label) : 1 }
        return label

    @staticmethod
    def sanatyze(string):
        """
        Avoid that dash or othes things splits the plugin name !
        below the waited format 
        in our case we set "plugin / type / type intance"  
        Host "/" plugin ["-" plugin instance] "/" type ["-" type instance]
        """
        return re.sub(r"[\$#\-\+!\*'\(\),\s\/\\]", '_', string).lower()
        #return re.sub(r"\-", "_", string).lower()

    @staticmethod
    def getRules(yamlFilePath):
        with open(yamlFilePath, "r") as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)


    def createMetric(self, Type, sample):
        """
        Sample : 
            Prometheus  :
            aws_applicationelb_active_connection_count_sum{job="aws_applicationelb",instance="",load_balancer="app/toto/f8be9abf9074a161",} 6.0 1642519620000 
            Graphite : 
            PLUGIN : aws_networkelb.TATA.TOTO    
            TYPE : <TypeProm>
            TYPE INSTANCE: un_healthy_host_count_average
        """
        for rule in collectdConfig['rules']:
            if re.search(rule['filter'], sample.name):

                # set type_instance, mandatory in prometheus exporter
                type_instance = sample.name
                if "metric" in rule:
                    type_instance = re.sub(rule['metric'].split(',')[0],rule['metric'].split(',')[1],sample.name)
    
                # set plugin with labels content, not mandatory in prometheus exporter 
                res = []
                if "labels" in rule:
                    for ConfLabel in rule['labels']:
                        ConfLabel = prometheusCollect.removeSign(ConfLabel)
                        newLabel = prometheusCollect.applyRules(ConfLabel, sample.labels)
                        #print(newLabel, ConfLabel, sample)
                        if newLabel != None:
                            res.append(newLabel)

                # Graphite output 
                # Host "/" plugin ["-" plugin instance] "/" type ["-" type instance]
                #       
                plugin = prometheusCollect.sanatyze('.'.join(res))
                # without label
                if len(res) == 0:
                    plugin = ""
                metric = collectd()
                metric.host = "cloudwatch"
                metric.plugin  = plugin
                metric.type = Type
                metric.type_instance = type_instance
                metric.values =  [sample.value]

                return metric

        return None

## Call

def config_func(config):
    '''
    get conf values
        Path : prometheus uri
        Rules : yaml file with rules
        Prefix : graphite prefix 
    '''
    path_set = False
    for node in config.children:
        key = node.key.lower()
        val = node.values[0]
        if key in collectdConfig.keys():
            collectdConfig[key].append(val)
        else:
            collectdConfig[key] = [val]

    # merge rules
    try : 
        collectdConfig['rules'] = prometheusCollect.getRules(collectdConfig["rules"])["jobs"]
        collectd.info('loaded rules file %s' % (collectdConfig["rules"]))
    except Exception as e:
        collectd.error('unable to get rules file %s: %s' % (collectdConfig["rules"], e))



def read_func():
    '''
    read values and put them to configured writer !
    '''
    # Read raw value
    #collectd.info('trace callback %s' % (prom_metrics))
    prometheusMetrics = prometheusCollect(collectdConfig["path"][0])
    prometheusMetrics.collect()

    for Type in prometheusMetrics.promCollect:
        for sample in prometheusMetrics.promCollect[Type]:
            metric = prometheusMetrics.createMetric(Type,sample)
            if metric == None:
                continue
            metric.dispatch()
 

'''
Start !
'''
collectd.register_config(config_func)
collectd.register_read(read_func)
